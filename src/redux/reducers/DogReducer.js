import { SET_DOG_IMAGE, SET_VALUE } from '../actions/Types';

const initialState = {
  image: null,
  value: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_DOG_IMAGE:
      return {
        ...state,
        image: action.payload,
      };
    case SET_VALUE:
      return {
        ...state,
        value: action.payload,
      };

    default:
      return state;
  }
};

import { combineReducers } from 'redux';
import dogReducer from './DogReducer';

const rootReducer = combineReducers({
  dogReducer,
});

export default rootReducer;

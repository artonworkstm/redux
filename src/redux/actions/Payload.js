import { SET_DOG_IMAGE, SET_VALUE } from './Types';

// Async API Function
export const setDogImage = () => {
  return async (dispatch) => {
    try {
      const response = fetch('https://dog.ceo/api/breeds/image/random');
      const data = await response.json();
      console.log('dispatching:', data.message);
      dispatch({
        type: SET_DOG_IMAGE,
        payload: data.message,
      });
    } catch (err) {
      console.log(err);
    }
  };
};

// Setter Function
export const setValue = (value) => (
  console.log(value),
  {
    type: SET_VALUE,
    payload: value,
  }
);

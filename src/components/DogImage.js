//import liraries
import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { useDispatch, useSelector } from 'react-redux';

import { setDogImage, setValue } from '../redux/actions/Payload';

// create a component
const DogImage = () => {
  const [input, setInput] = useState('');
  const actions = bindActionCreators({ setDogImage, setValue }, useDispatch());
  const {
    dogReducer: { image, value },
  } = useSelector((state) => state);
  return (
    <div>
      <div>
        <h1>{value}</h1>
        <input
          type="text"
          value={input}
          onChange={(value) => setInput(value.target.value)}
        />
        <input
          type="button"
          value="Set value"
          style={{ border: 'none' }}
          onClick={() => actions.setValue(input)}
        />
      </div>
      <div>
        <img
          src={{ uri: image }}
          style={{ width: '50%' }}
          alt="Error loading.."
        />
        <input
          type="button"
          value="Get Dog"
          style={{ border: 'none' }}
          onClick={() => actions.setDogImage}
        />
      </div>
    </div>
  );
};

//make this component available to the app
export default DogImage;
